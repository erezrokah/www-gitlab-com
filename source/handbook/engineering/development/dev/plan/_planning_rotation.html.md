To assign weights to issues in a future milestone, we ask team members to
continually weight and break-down issues in ~workflow::planning breakdown that don't have a ~"Breakdown Sufficient" label,
continually weight and break-down issues in ~workflow::planning breakdown,
especially pieces of work in which they have experience or which belongs to 
their group.

Contributions that add new information or insight are welcome, even if they
don't consistute a complete break-down. When a discussion fails to meet a 
conclusion in a timely manner, include the PM immediately so they can 
clarify requirements or cut scope.

Often new complexity is revealed when development starts or as it progresses.
This is normal. Team-members should re-assess weights when new information 
becomes clear and alert the PM or EM when delivery within the milestone is
at risk.

To weight issues, team-members should:

1. Look through the issues in [~"workflow::planning breakdown" in the upcoming milestone](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=workflow%3A%3Aplanning%20breakdown&milestone_title=%23upcoming).
2. For those they understand, add a weight and a short comment explaining why 
   that weight is appropriate, what parts of the code they think would be
   involved, and any risks or edge cases we'll need to consider.
3. For issues requiring more information, leave an initial insight and copy
   in team-members who are likely to be able to help.
4. Bias toward adding a weight rather than achieving the perfect breakdown.
   The process is intended to be lightweight and weights will often be revised 
   anyway. Don't let discussions hang.
5. If two people disagree on the weight of an issue, even after explaining their
   perceptions of the scope, use the higher weight.
6. If the weight is high **(4 or 5)** or the issue is **unlikely to be completed
   in a single milestone**, alert the PM so that scope can be cut. If possible,
   help them by suggesting a technically simpler iteration.

