---
layout: handbook-page-toc
title: "Sales Training"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# **GitLab Sales Learning Framework**
GitLab sales team members are expected to be knowledgeable and proficient across a variety of topics, skills, behaviors, and processes. For ease of consumption, sales training resources are organized in the following 6 categories below. If you are looking for more immediate access to resources to support your selling efforts, please check out the [Sales Resources](/handbook/marketing/product-marketing/sales-resources/) page.

| **Category** | **Description** |
| ---------------------------------------- | ----------------------------------------- |
| **[GitLab Target Audiences](/handbook/sales/training/#gitlab-target-audiences)** | Successful sales team members must have an intimate understand of ideal customer profiles, customer environments where opportunities for GitLab are most ripe, targeted buyer personas, industry-relevant insights, and more |
| **[Industry Topics & Trends](/handbook/sales/training/#industry-topics--trends)** | To serve as a trusted advisor, GitLab sales team members must be able to competently and confidently engage with customers on topics like SDLC, SCM/VCS, open source & open core, DevOps, CI/CD, containers, Kubernetes, and more |
| **[Why GitLab](/handbook/sales/training/#why-gitlab)** | GitLab sales team members must be able to clearly articulate a compelling, differentiated, value-driven message to customers, prospects, and partners |
| **[Competition](/handbook/sales/training/#competition)** | To maximize opportunities, GitLab sales team members must demonstrate the knowledge and ability to beat out various competitors |
| **[GitLab Portfolio](/handbook/sales/training/#gitlab-portfolio)** | GitLab sales team members must understand how various elements of the GitLab portfolio (offering tiers, SDLC phases, services, training, etc.) solve customer challenges |
| **[Functional Skills & Processes](/handbook/sales/training/#functional-skills--processes)** | To optimize productivity, GitLab sales team members must be able to consistently demonstrate the sales skills and behaviors (including adherence to standard processes and use of tools like Salesforce and others) that lead to desired outcomes |

## **GitLab Target Audiences** 

| **Topic** | **Resource(s)** |
| ------ | ------ |
| What is GitLab’s ideal customer profile? | - [Handbook page](/handbook/marketing/revenue-marketing/account-based-marketing/#ideal-customer-profile) |
| What business problems does GitLab help customers solve? | - Check out the value drivers and other materials on the GitLab [Command of the Message page](/handbook/sales/command-of-the-message/) | 
| What are target buyer personas and their pain points? | - [Understanding the GitLab Buyer: Personas and Pain Points](https://youtu.be/-UITZi0mXeU) (Jan 2018, 10 minutes) <br> - [GitLab Buyer Personas](/handbook/marketing/product-marketing/roles-personas/#buyer-personas) <br> - Learn more about [Enterprise IT roles & key pain points](/handbook/marketing/product-marketing/enterprise-it-roles/) |
| What are target user personas and their pain points? | - [Handbook page](/handbook/marketing/product-marketing/roles-personas/#user-personas) | 

## **Industry Topics & Trends**

| **Topic** | **Resource(s)** |
| ------ | ------ |
| What is DevOps? | - [DevOps Explained](/devops/) (20 minutes) <br> - [Video](https://www.youtube.com/watch?v=_I94-tJlovg) (Dec 2013, 7 minutes) <br> - [Stages of the DevOps Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) <br> - [DevOps terms: 10 essential concepts, explained](https://enterprisersproject.com/article/2019/8/devops-terms-10-essential-concepts) <br> - [Key Findings of Annual DORA (DevOps Research & Assessment) Survey](https://www.youtube.com/watch?v=VBsQmE8LIYM) (Dec 2018, 12 minutes) <br> - [Understand the Industry where GitLab Competes](https://www.youtube.com/watch?v=qQ0CL3J08lI) (Jan 2018, 11.5 minutes) |
| What is the software development life cycle and how is software developed and deployed? | - [GitLab's public website on the SDLC](/sdlc/) <br> - [What is the Software Development Lifecycle (SDLC)?](https://www.youtube.com/watch?v=Ancdhr3t2sE) (July 2018, 11 minutes) <br> - [Software Development Lifecycle (SDLC) in 9 minutes!](https://www.youtube.com/watch?v=i-QyW8D3ei0) (Jan 2016, 9 minutes) <br> - [How is software made using GitLab?](https://www.youtube.com/watch?v=UuX-GnYWNwo&feature=youtu.be) (Nov 2019, 23 minutes)|
| What is Git? | - [Git Tutorials](https://www.youtube.com/playlist?list=PLu-nSsOS6FRIg52MWrd7C_qSnQp3ZoHwW) |
| What is cloud-native? | - [GitLab website](https://about.gitlab.com/cloud-native/) <br> - [Video on Cloud-Native Transformation](https://www.youtube.com/watch?v=WsIM034RnAc) (Mar 2019, 29 minutes) |
| What is SCM? | - [Video](https://drive.google.com/file/d/13YIChJtWWTI2ekfM3ClSi0DMZRFpaQ9b/view) (June 2019, 29 minutes) |
| What is CI? |  |
| What is CD? |  |
| What is DevSecOps? |  |
| What is GitOps and Infrastructure as Code? |  |
| What is shift left? |  |
| What is open source? |  |
| What is open core? |  |
| What are microservices? | - [Video: What are Microservices?](https://www.youtube.com/watch?v=petnTitp6CQ) (Jan 2019, 19 minutes) |
| What is serverless? | - [What is Serverless?](https://www.youtube.com/watch?v=GBOroGozm5w) (Jan 2019, 18 minutes) | 
| What are containers? |  |
| What is Kubernetes? | - [The Evolution of the Kubernetes Landscape](https://www.youtube.com/watch?v=5OLkPK4MRVQ) (Nov 2019, 30 minutes) |
| What is complete DevOps? |  |
| What is concurrent DevOps? | - [Video](https://www.youtube.com/watch?v=bDTYHGEIeM0) (Apr 2019, 24.5 minutes) <br> - [GitLab Concurrent DevOps white paper] ([internal link](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/resources/downloads/gitlab-concurrent-devops-whitepaper.pdf)) ([gated link](https://about.gitlab.com/resources/whitepaper-concurrent-devops/)) |
| How do I keep up to date with industry trends? | - Review and subscribe to the following blogs: [Hacker News](https://news.ycombinator.com/), [Martin Fowler](https://martinfowler.com/), and [New Stack](https://thenewstack.io/) | 

## **Why GitLab?**

| **Topic** | **Resource(s)** |
| ------ | ------ |
| What is GitLab? | - [Video](https://www.youtube.com/watch?v=MqL6BMOySIQ) (Sep 2018, 3 minutes) <br> - [GitLab infomercial](https://youtu.be/gzYTZhJlHoI) featuring David Astor (Feb 2020, 5.5 minutes) |
| Key value drivers and differentiators | - Review the [Command of the Message](/handbook/sales/command-of-the-message/) page | 
| Positioning GitLab | - GitLab customer deck ([presentation](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing)) <br> - GitLab customer deck ([video](https://youtu.be/UdaOZ9vvgXM) (Dec 2019, 28 minutes) <br> - [GitLab value prop messaging](/handbook/marketing/product-marketing/messaging/#gitlab-value-proposition) <br> - [Pitch deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit?usp=sharing) <br> - [Delivering the GitLab pitch deck](https://drive.google.com/open?id=1vRgU1o-o4kcOblQCxNi3h6xrN7KQZY1H) (Apr 2019, 14 minutes) <br> - [Keys to Faster Delivery (Accelerate Digital Transformation)](https://youtu.be/MwSJuKYXAy4) (Jul 2019, 18.5 minutes) <br> - [Removing Software Bottlenecks](https://www.youtube.com/watch?v=RT-fKTFevEY) (Mar 2019, 30 minutes)| 
| GitLab culture | - [GitLab Culture](https://about.gitlab.com/company/culture/) <br> - [GitLab Values](/handbook/values/) <br> - [This is GitLab video](https://www.youtube.com/watch?v=Mkw1-Uc7V1k) (Feb 2018, 3.5 minutes) <br> - [Everyone Can Contribute video](https://www.youtube.com/watch?v=V2Z1h_2gLNU) (May 2019, 3.5 minutes) | 
| GitLab subscription model | - [How GitLab.com Subscriptions Work](https://www.youtube.com/watch?v=W-ZYi4H4XMM) (Jun 2019, 29 minutes) | 
| GitLab vision | - Review [GitLab’s Direction](/direction/#single-application) |
| Follow GitLab | - Review and subscribe to the [GitLab Blog](/blog/) | 

## **Competition**

| **Topic** | **Resource(s)** |
| ------ | ------ |
| Who are GitLab's competitors? | - [GitLab Comparison Page](/devops-tools/) |
| What is Competitive Intelligence? | - Check out the [Handbook page](https://about.gitlab.com/handbook/marketing/product-marketing/competitive/) |
| Microsoft Azure DevOps | - [Proactively Competing Against Microsoft](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit?usp=sharing) presentation (SKO 2020, internal only) <br>- [Proactively Competing Against Microsoft](https://www.youtube.com/watch?v=Ds_U8iiUOz8) private video (Feb 2020, 58 minutes) <br - [Azure DevOps Competitive Review: Part 1 of 2](https://www.youtube.com/watch?v=T-zfXQpvtAw) (Nov 2018, 23 minutes) <br> - [Azure DevOps Competitive Review: Part 2 of 2](https://www.youtube.com/watch?v=NeMzSOg7dV4) (Nov 2018, 13 minutes) |
| GitHub | - [First Dive into GitHub Actions](https://youtu.be/RlT5tAALm5w) (Nov 2019, 38 minutes) |
| Jenkins | - [Proactively Competing Against Jenkins](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg) presentation (SKO 2020, internal only) <br> - [Competing Against Jenkins and CloudBees](https://youtu.be/a95DQqRTOHw) (Sep 2019, 25 minutes) <br> - [Jenkins: Competitive Overview](https://www.youtube.com/watch?v=_HJMtp-fcHc) (Feb 2019, 31 minutes) <br> - [Jenkins: Overcoming Objections](https://www.youtube.com/watch?v=qrENxT9iPvE) (Mar 2019, 26 minutes) <br> - [GitLab vs Jenkins, where is GitLab _weaker_?](https://www.youtube.com/watch?v=3Wr3O6mY5VE) (Feb 2019, 25.5 minutes) |
| Where may one find additional competitive insights? | - [Crayon Market and Competitive Intelligence Tool Launch](https://youtu.be/qCKj6zB-Ebk) (Sep 2019, 30 minutes) <br> - [Competitive Battlecards on Crayon](https://app.crayon.co/intel/gitlab/battlecards/) (log in via Okta SSO!) |

## **GitLab Portfolio**
*  [GitLab Product Vision](https://www.youtube.com/watch?v=RCMr7i3zSwM) (Apr 2019, 30 minutes)
*  Review [GitLab’s Direction](/direction/#single-application)
*  [GitLab Plan to Monitor Demo v.11.3](/handbook/marketing/product-marketing/demo/i2p/) (20 minutes)
*  [The GitLab Customer Journey and the CI Use Case](https://youtu.be/JoeaTYIH5lI) (Mar 2020, 30 minutes)
*  [Use Case Session 1: Increase the quality of my code while decreasing time to delivery (Continuous Integration (CI))](https://youtu.be/us1hjgfX8hE) (Sep 2019, 26.5 minutes)
*  [How to upgrade your customer to a higher tier](https://www.youtube.com/watch?v=8ZpU7PZzFyY) (Jul 2019, 26 minutes)
*  [GitLab Ultimate is Ready for Primetime](https://www.youtube.com/watch?v=3M8SIeykbrM) (Nov 2019, 31 minutes)
*  [Agile Project & Portfolio Management on GitLab Click-Through Demo](https://www.youtube.com/watch?v=Eo8pFoE6DjU) (Jun 2019, 30 minutes)
*  [GitLab AutoDevOps Run Click-Through Demo](https://www.youtube.com/watch?v=V_6bR0Kjju8) (Apr 2019, 21 minutes)
*  [Q3FY20 GitLab Release Update for Sales](https://www.youtube.com/watch?v=y2KrovJD76Q) (Aug 2019, 29 minutes)
*  [Forrester Wave Cloud CI Report: What It Means to GitLab Sales, Customers & Prospects](https://youtu.be/q6YXIvHZuDU) (Oct 2019, 32 minutes)
*  [Selling Services to Accelerate Customer Adoption](https://youtu.be/ngTI5-1cQK4) (Oct 2019, 28 minutes)
*  Security
   - [GitLab Security & Compliance Capabilities](https://www.youtube.com/watch?v=-e4V9j0g80A) (Jan 2019, 37 minutes)
   - [Forrester Software Composition Analysis (SCA) Wave & Security Q&A](https://www.youtube.com/watch?v=FIfZCg02G3o) (Apr 2019, 30 minutes)
*  [What is SCM?](https://drive.google.com/file/d/13YIChJtWWTI2ekfM3ClSi0DMZRFpaQ9b/view) (June 2019, 30 minutes)

## **Technical Skills**
* [First Steps to Becoming Certified Kubernetes App Developer](https://www.katacoda.com/courses/kubernetes/first-steps-to-ckad-certification) (30-60 minutes)
* [Kubernetes API Fundamentals](katacoda.com/openshift/courses/operatorframework/k8s-api-fundamentals) (30 minutes)

## **Functional Skills & Processes**
*  [Field Functional Competencies](/handbook/sales/training/field-functional-competencies/)
*  [Reference Edge - Customer Reference Process](https://youtu.be/8Le_Ovglnq8) video (Jan 2020, 25 minutes)
*  [Critical GitLab Sales Skills and Behaviors](https://youtu.be/K3h33xFXpow) video (Jul 2019, 9.5 minutes)
*  [Anatomy of a Successful Discovery Call](https://youtu.be/maai4tuDtoM) video (Jul 2019, 21 minutes)
*  [Things I Wish I Knew During My First Few Quarters at GitLab](https://youtu.be/3gprWrDTEQM) video (Aug 2019, 25.5 minutes)
*  Study and bookmark the [Sales Handbook page](/handbook/sales/)
*  [Sales Forecasting at GitLab](/handbook/sales/#forecasting)
*  [Version.GitLab.Com Walk-Through](https://www.youtube.com/watch?v=lBWwlbd1J5k) (Jul 2019, 8.5 minutes)
*  Chorus
   - [How to find a call in Chorus](https://hello.chorus.ai/listen?guid=10c8460049f842e99477de4e9f2affca) (Jul 2019, 4 minutes)
   - [How to review a call in Chorus](https://hello.chorus.ai/listen?guid=5fc22bca9ec04141a5245062b7907f09) (Jul 2019, 7 minutes)
   - [How to comment and share moments in Chorus](https://hello.chorus.ai/listen?guid=199654f39cbd4fc69c00e839ea406930) (Jul 2019, 5 minutes)
   - [How to gain compliance on recorded calls (primarily for NA)](https://hello.chorus.ai/listen?guid=6d8a5c1bcb364547bb8dc2eed8a5e6a5) (Jul 2019, 6 minutes)
*  [Clari Sales Forecasting & Pipeline Management Tool](https://app.clari.com/)
   - Join the [Google Classroom](https://classroom.google.com) for in-depth training 
   - On the top right of the Classes page, click + then  > Join class
   - Enter the Class Code: **e7hfgg0** 
*  Social Selling Basics
   - [Social Selling Basics presentation](https://docs.google.com/presentation/d/1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A/edit?usp=sharing)
   - [Social Selling Basics video](https://youtu.be/w-C4jts-zUw) (Jul 2019, 20 minutes)
      - [Social Selling_Sales Enablement_2019-07-11](https://www.youtube.com/watch?v=Ir7od3stk70) (Jul 2019, 28 minutes)
   - [LinkedIn Sales Navigator resources](https://docs.google.com/document/d/1UF69ieck4AdHadzgPmZ5X1GBs3085JhlYaMowLj0AOg/edit?usp=sharing)
*  [How to Host a GitLab Meetup](https://www.youtube.com/watch?v=h24aS5rpfmM) (May 2019, 36.5 minutes)
*  [Just Commit marketing campaign overview](https://www.youtube.com/watch?v=58qDalA5o6Q) (Feb 2019, 11.5 minutes)
*  [How to Avoid Death by PowerPoint](https://www.youtube.com/watch?v=Iwpi1Lm6dFo) (20 minutes)
*  Proof of Value for SAs
   -[Proof of Value Review](https://docs.google.com/presentation/d/1LpvXLtviKOlDay5p3XtgDsWDW8x8WS8_SF9qT13zkoU/edit#slide=id.g153a2ed090_0_63)
* Customer Onboarding for TAMs
   -[Customer Onboarding Custom Object Video](https://drive.google.com/file/d/17Y8LpYTaLwIByyd-BKN9e8u80oxaqFtt/view) (Nov 2019, 17 minutes)
   -[Customer Onboarding Custom Object Deck](https://docs.google.com/presentation/d/1SBQ_FbCsPfycTxdtoracQTAh7WOpGPPAqptXfRiZdvc/edit#slide=id.g29a70c6c35_0_68)
   -[Customer Onboarding SalesForce Dashboard](https://gitlab.my.salesforce.com/?ec=302&startURL=%2F01Z4M000000skrQ)
*  Gainsight for Technical Account Managers
   - [Gainsight Foundations](https://education.gainsight.com/gainsight-foundations-nxt-fka-101/315512) (26 minutes)
     This video provides an overview for of Gainsight NXT Platform, and provides a brief demo of key capabilities.
   - [Gainsight Reports and Dashboards](https://education.gainsight.com/series/end-user-training-for-nxt/gainsight-reports-dashboards-nxt-replaces-102) (19 minutes)
     This video provides an overview of how to leverage dashboards within the tool to make better decisions on managing your accounts.
   - [Calls to Action (CTA), Playbooks, and the Cockpit](https://education.gainsight.com/series/end-user-training-for-nxt/gainsight-cockpit-playbooks-nxt-replaces-102) (23 minutes]
     This video provides an overview of Calls to "Action (CTWAs) in the Cockpit, which is the tool to manage which customers to reach out to when and why,  as well as Playbooks that recommend tasks to complete as part of CTA.
   - [360 View, Health Scores, and Timeline](https://education.gainsight.com/series/end-user-training-for-nxt/gainsight-360-timeline-nxt-replaces-102) (34 minutes)
     This video provides an overview of the 360 feature for research and call prep, Timeline for centralized note-taking, and an overview of health scores with an eye for trends over time.
  - [Sally Bot in Slack](https://education.gainsight.com/series/end-user-training-for-nxt/gainsight-sally) (15 minutes)
     This video provides an overview of what Sally is and how to use Sally in Slack

## Top FAQ’s Asked by Salespeople

<table>
  <tr>
   <td><strong>Category</strong>
   </td>
   <td><strong>Question</strong>
   </td>
   <td><strong>Answer</strong>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>What authentication options are available when using .com?
   </td>
   <td><a href="https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html">Check out SCIM</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>What are the best ways to achieve high availability?
   </td>
   <td><a href="https://about.gitlab.com/solutions/jira/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>What are the best ways to achieve disaster recovery?
   </td>
   <td><a href="https://docs.gitlab.com/ee/administration/geo/replication/index.html">With GitLab GEO</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>What are the GitLab security capabilities?
   </td>
   <td><a href="https://docs.gitlab.com/ee/user/application_security/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>How does GitLab CI work?
   </td>
   <td><a href="https://docs.gitlab.com/ee/ci/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>How can we do test management with GitLab?
   </td>
   <td><a href="https://about.gitlab.com/direction/plan/quality_management/">We are exploring enhancements in this area</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>What options do we have to manage user access on .com?
   </td>
   <td><a href="https://docs.gitlab.com/ee/user/group/saml_sso/">You will want to consult group SAML</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>When will we no longer rely on using Docker-in-Docker (DinD) for security scanners?
   </td>
   <td><a href="https://gitlab.com/gitlab-org/gitlab/-/issues/37278">Current effort ongoing</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>What best practices exist around using projects/groups?
   </td>
   <td><a href="https://www.youtube.com/watch?v=VR2r1TJCDew">Look at this video</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>How do runners actually work?
   </td>
   <td><a href="https://docs.gitlab.com/runner/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Features
   </td>
   <td>What can we use the package repositories in GitLab for? Will they replace Nexus or Artifactory?
   </td>
   <td><a href="https://about.gitlab.com/devops-tools/jfrog-artifactory-vs-gitlab.html">Check out the comparison</a>
   </td>
  </tr>
  <tr>
   <td>Integrations
   </td>
   <td>How can we integrate Jira?
   </td>
   <td><a href="https://about.gitlab.com/solutions/jira/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Integrations
   </td>
   <td>How does your out-of-the-box support for (AWS/Azure/GCP) looks like?
   </td>
   <td><a href="https://about.gitlab.com/multicloud/">Please look at our multi cloud support</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>Show us some reference architectures together with sizing information.
   </td>
   <td><a href="https://docs.gitlab.com/ee/administration/high_availability/README.html#reference-architectures">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>How should I set up and customize user roles in GitLab?
   </td>
   <td><a href="https://docs.gitlab.com/ee/development/permissions.html">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>What are the best practices for backing up a GitLab instance?
   </td>
   <td><a href="https://docs.gitlab.com/ee/raketasks/backup_restore.html">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>How can we move from SVN to git?
   </td>
   <td><a href="https://docs.gitlab.com/ee/user/project/import/svn.html">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>How can we auto-provision users?
   </td>
   <td><a href="https://docs.gitlab.com/ee/user/profile/account/create_accounts.html#create-users-through-integrations">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>What are best practices for a runner infrastructure?
   </td>
   <td><a href="https://docs.gitlab.com/runner/best_practice/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>What installation method for GitLab should we use?
   </td>
   <td><a href="https://about.gitlab.com/install/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Implementation
   </td>
   <td>What are best practises for a developer workflow?
   </td>
   <td><a href="https://docs.gitlab.com/ee/topics/gitlab_flow.html">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Product Tiering
   </td>
   <td>How to do a migration from CE to EE?
   </td>
   <td><a href="https://about.gitlab.com/upgrade/">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Product Tiering
   </td>
   <td>How to do a migration from self-managed to .com?
   </td>
   <td><a href="https://docs.gitlab.com/ee/user/project/import/#migrating-from-self-managed-gitlab-to-gitlabcom">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Product Tiering
   </td>
   <td>What is the difference between the Jira integration in Starter and in Premium?
   </td>
   <td><a href="https://docs.gitlab.com/ee/integration/jira_development_panel.html">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Product Tiering
   </td>
   <td>What are the differences around merge request approvals between the EE tiers?
   </td>
   <td><a href="https://about.gitlab.com/features/#create">Resource</a>
   </td>
  </tr>
  <tr>
   <td>Product Tiering
   </td>
   <td>How is user management different on .com compared to self-managed?
   </td>
   <td>Resource
   </td>
  </tr>
</table>


# **Sales Enablement Sessions**
Live sales enablement videocasts are held every Thursday from 12:00-12:30pm ET with the exception of the last two weeks of every quarter. Sessions are recorded and published to YouTube, made available for on-demand playback, and added to the inventory above. [Learn more here](/handbook/sales/training/sales-enablement-sessions/).

# **Customer Success Skills Exchange**
Live customer success enablement videocasts are held every Wednesday, and delivered on a weekly basis alternating time slots every other week. Every two weeks they are delivered at 10:30-11:20am ET, and the alternating weeks are delivered at 2:00-2:50pm ET. Sessions are recorded and published to [YouTube](https://www.youtube.com/playlist?list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_), and made available for on-demand playback.

# **SDR Coaching**
Similar sessions are held for GitLab’s SDR team every Wednesday from 12:30-1:00pm ET. Learn more [here](/handbook/sales/training/sdr-coaching). 

# **Additional Notes**
*  Learn about GitLab customer training on the [GitLab Training Tracks site](/training/)
*  Learn about the [weekly GitLab Sales Enablement webcast series](/handbook/marketing/product-marketing/enablement/)
*  Learn about the [weekly GitLab SDR Enablement webcast series](/handbook/marketing/product-marketing/enablement/#xdr-bdrsdr-coaching)
