---
layout: handbook-page-toc
title: "Facilitate The Opportunity"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


<h2>Facilitate the Opportunity</h2>

You have qualified a lead and now you need to turn it into a sale. This phase is called **Facilitate the Opportunity** with the goal of getting a verbal **Commit** from the customer. 

In this phase there are 4 high level tasks you need to complete to get a customer Commit: **Discovery,** **Scoping,** **Technical Evaluation**, and **Pitching** the GitLab solution.

Again the four tasks above are demonstrable outputs of correctly applying **MEDDPICC **and **COM** within each of the tasks. This also supports your transition from appearing as a common software sales person into a trusted consultant that brings business solutions instead of multiple invoices and a smile. 

<h3>Step 1: Discovery Questioning</h3>

Traditional sales typically bypasses understanding the customer. Traditional sales relies heavily on **getting a foot in the door** with a customer by overwhelming them with product information in the hopes that a buzz word or some random product feature will catch the customer’s attention and build urgency in the customer. GitLab doesn’t operate that way. GitLab uses Discovery Questioning. 

[Discovery Questions](https://drive.google.com/a/gitlab.com/open?id=1balLINV-vnd6-6TYzF3SIIJKrBPUQofVSDgQ5llC2Do) are designed to identify existing needs, problems, customer pain points, customer’s goals etc. Discovery questions are also designed to ferret out what the known unknowns the customer has about their own business environment and business goals. Regardless of the opportunity being handed off from an SDR or qualified by you, you should always create discovery questions based on the following research process and criteria.

<h4>Step 1.1: Research</h4>

**Step 1.1.1 [Determine your Buyer Persona](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/)** - a buyer persona is a fictional representation of your key buyers. This helps you pre-identify obstacles, objections, initiatives, and metrics that are important to your buyer. This step will give you an edge in understanding your customers better than your competition. There are typically 5 buyer personas you may encounter.

*   Technology Leader
*   Business Application Leader
*   Security Leaders
*   COO/CXX Leader
*   Financial Leader

**Step 1.1.2 Reference Buyer Process Maps** (BPMs) - are maps of mental pathways buyers’ travel to make decisions. BPMs will guide selling activity based on where the buyer is in their process. Your goals in this step is differentiation, reduction in resistance, avoid wasting cycles, and facilitate better meetings and create **Moments that Matter**. See a breakdown of the [Buyer Process Map](https://docs.google.com/presentation/d/181vsu60-UYk1890BJjemfjZ3GIQhYrPrsNAh4xN3-wM/edit#slide=id.p25) stage and the common buyer actions that indicate what stages of the buyer process the prospect is in.

**Step 1.1.3  Research the Buyer** - includes a number of sub-tasks necessary to get additional insight into the buyer.

1. Reference **SFDC** to see if the lead is a current customer or prospect. Review their digital body language such as recent activities and past contact.
2. Reference **LinkedIn** and cross reference the contact’s profile to relevant buyer personas and look for overlaps.
3. Reference the **company’s website** and absorb basics related to strategic goals, industry, business model, their buyer, etc.

**Step 1.1.4 Create Discovery Questions according to [MEDDPICC](https://about.gitlab.com/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification)** to gather the information necessary to qualify the opportunity. 

*   [Discovery Questions](https://drive.google.com/a/gitlab.com/open?id=1balLINV-vnd6-6TYzF3SIIJKrBPUQofVSDgQ5llC2Do) are designed to identify existing needs, problems, customer pain points, customer’s goals etc. 
*   MEDDPICC is a qualification framework that will help you determine if the opportunity is worth your time, effort, and company resources. This is an aspect of **Pre-Call Planning. **The pre-call planning questions will cause you as the seller to further validate and reference the buyer persona you will engage with. It will help you to more quickly determine what stage the buyer falls in the sales process. The framework will also expose problems and clarify next steps. It will help make you more efficient and focused sales representatives by making it easier to sort the good deals from no deals, and quick deals from long term deals. 

**Step 1.1.5 Develop a Persona Based Call Plan** to ensure that you gather more pertinent information from the buyer. It will help identify information gaps, set specific objectives, and ultimately increase your buyer’s confidence in you. Use the two job aids below to prepare for your discovery calls.

*   Use [Sales Discovery and Qualification Questions](https://about.gitlab.com/handbook/sales/qualification-questions/) to create questions that are tailored to the specific buyer persona you’ll be engaging with. 
*   Use the [Customer Requirements Document ](https://docs.google.com/spreadsheets/d/1y1nk7TmjxOxaO8nUg6w1YO04X9wwtz39MnP7NHXq6ec/edit#gid=0)to list all of your questions as part of your discovery call meeting preparation. 

**Step 1.1.6 Conduct Discovery Call Meetings**

Discovery doesn’t take place in just one call. It takes place over a series of meetings. It’s important to take the time to get to understand the customer’s organization on an intimate level in order to propose the best product solution possible. Below are the tasks and assets you’ll need to facilitate successful discovery call meetings.

1. **Prepare a Meeting Agenda** to identify how the buyer wants will match what you will cover. This can be sent ahead to the buyer to ensure alignment. For each agenda item, include some questions from the BPM, prepare statements, industry case studies. Research 1-3 items in the Objectives and Obstacles section of the personas. Finalize your meeting objectives in order to show the buyer you are prepared, listening, and empathic. Structure the meeting so that the prospect is relaxed and is comfortable going deep with their problems and needs.
2. Utilize the **[Customer Requirements Document](https://docs.google.com/spreadsheets/d/1y1nk7TmjxOxaO8nUg6w1YO04X9wwtz39MnP7NHXq6ec/edit#gid=0)** during the meeting to capture the feedback from your discovery questioning.
3. Utilize a **Command Plan** to track the progress of a deal. It’s an iterative document within Salesforce that gives you and your team macro and micro level visibility into an opportunity. It is designed to align and guide you through the necessary MEDDPICC process to quickly capture the information necessary to properly qualify an opportunity. Here is a sample [Command Plan](https://docs.google.com/spreadsheets/d/16m5_w4NiNSXfJFt9576IktNUp6uTCOKxaLBtoyR0lrA/edit#gid=299910663).

The data gathered from the discovery calls is analyzed by **AE/SAL** to make the determination of whether to qualify the lead in terms of GitLab value to their organization. Upon determination of qualifying, the AE/SAL then should engage a **Solution Architect** to begin the transition to the scoping phase of the sales process. 

**Step 1.1.7 AE/SAL Transition to Solution Architect**



1. **AE/ SAL performs a Gap Analysis** of data captured from discovery questioning and is held in the Customer Requirements document. Align customer requirements on the spreadsheet to the specific GitLab product line and denote if GitLab does or does not meet the requirement. This analysis will also provide you with possible devOps cycle entry points as customer requirements and goals do vary.
2. **AE/SAL introduces and engages** the solution architect in technical conversations and reviews Customer Requirement Document with solution architect. 
3. **AE/SAL updates relevant SFDC** fields with new information for formal handoff to the solutions architect for **Scoping**. Utilize a **Command Plan** to track the progress of a deal. It’s an iterative document within Salesforce that gives you and your team macro and micro level visibility into an opportunity. 

<h3>Step 2: Scoping</h3>

Scoping represents an opportunity to discuss customer requirements in more detail. This step is the primary responsibility of a **Solutions Architect (SA)**.  The SA will do a deep-dive technical requirements review based on the documents provided by the AE/SAL. The SAs main goal in scoping is to understand all the necessary elements that need to be brought into the project scope and identify the pieces that need to be brought together in order to produce a successful outcome for all stakeholders. The tasks necessary to accomplish proper scoping are the following:

1. **Review the Customer Requirements** document and any associated meeting notes to identify gaps and create more indepth discovery questions related to technical capabilities. 
2. **Perform high-level technical discovery calls** with the customer and work together to review the [Customer Requirements Document](https://docs.google.com/spreadsheets/d/1y1nk7TmjxOxaO8nUg6w1YO04X9wwtz39MnP7NHXq6ec/edit#gid=0) and gather more context and insight into the customer’s requirements. 
3. **Analyze** the updated requirements against GitLab products to determine a fit assessment. If a fit is determined, initiate a Technical Evaluation.

<h3>Step 3: [Technical Evaluation](https://about.gitlab.com/handbook/customer-success/comm-sales/customer-buyer-journey.html#step-2-technical-evaluation)</h3>


Technical evaluation is where the prospective customer evaluates the fit of GitLab to their organization and their specific business outcomes. This may be driven by a Lite POV, a free trial, real time or asynchronous Q&A, a workshop and/or other approach requiring technical guidance from an SA. If Discovery did not include the SA, the AE should follow the triage process to engage an SA for the opportunity, filling out the issue template and providing MEDDPICC information as well as the known required capabilities in Salesforce. The following are tasks the SA must review and manage during technical evaluation. 



1. **Update** Customer Requirements Documents and review Google Docs and Salesforce for running meeting notes with customer environment and technical specifications.
2. **Setup** a free trial of GitLab based on the following [Technical Evaluation Criteria](https://about.gitlab.com/handbook/customer-success/comm-sales/customer-buyer-journey.html#responsibilities-1).
3. **Create** a solution design blueprint from requirements gathering, tech discovery, and customer meta-records.
4. **Create** a [Proof of Value](https://about.gitlab.com/handbook/sales/POV/) when necessary which focuses on specific customer business outcomes that cannot be achieved through other technical and/or business interactions.
5. **Submit** Proof of Value (PoV/PoC) plan to stakeholders for approval.

Once the SA has completed the solution design blueprint and/or the Proof of Value, the sales representative will need to hold a strategy meeting with the SA to discuss the highpoints of the solution in order to prepare and present a pitch deck to the customer. If the sales representative does not have deep technical product depth, it will be even more  important to debrief the solution blueprint with the SA. This debrief will help provide the sales representative with the information to have Command of the Message.  [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) is defined as "being audible ready to define solutions to customers’ problems in a way that differentiates you from your competitors & allows you to charge a premium for your products & services.” This means you must in many ways understand the customers pain points and goals better than they do and speak to real solutions. The solution is presented to the customer through a pitch deck.

<h3>Step 4: Pitching a Solution</h3>

The goal of developing and presenting a good pitch is to obtain a **verbal commit **from the customer. Again, traditional sales representatives tend to overwhelm customers with a long overview of the company and its product features in the hopes that one of the features will resonate with the customer and create a sense of urgency to buy. **Always avoid making a pitch more about us than the customer. ** At GitLab the approach is more consultative. This is the reason that insight into the customer based on individual research and discovery calls is so important. It gives you an intimate understanding of what the customer’s problems are and their long term goals. Your responsibility in developing and presenting a GitLab sales pitch is to align the customer’s problem to a particular GitLab product based solution. You have worked with the customer to this point to do the heavy lifting and understand what they need. Now as a consultant you are presenting a solid solution to meet their immediate needs but also providing a solution roadmap of their long term goals and needs.  **As a rule, a GitLab Sales Pitch deck should be no more than 7 slides**. You can use  the [GitLab Customer Template](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit#slide=id.g6bb3f57e89_0_50) and reduce it to the recommended number of slides. It is also recommended that the presentation be for visual rather than text heavy. A GitLab pitch deck should contain the following four components:

1. **Value Drivers**
2. **How GitLab can Help**
3. **Case Study that aligns with the customer**
4. **Ask for the Commit/Next Steps**

<h4>1 Align Customer Problems with Value Drivers</h4>

As a GitLab sales representative you should know from your discovery calls and research your customers pain points well enough to match them to one if not all of the GitLab Value Drivers. GitLab has the following three value drivers:

*   Increase Operational Efficiencies
*   Deliver Better Products Faster
*   Reduce Security and Compliance Risk

Your job is to call out the specific key pain points and show the customer how their pain aligns with GitLab’s value drivers. 

<h4>2 Show How GitLab Can Help</h4>

The next component of the pitch deck is showing how GitLab can help. This will require you to understand the customer’s specific use case. You discovery calls should have provided much of the data to define the problem, but going further you must do the following:

*   Research [Customer Use-Cases](https://about.gitlab.com/handbook/use-cases/#)
*   Align the customer’s pain point to a typical use-case
*   Explain what Gitlab does in relation to the associated use-case

This part of the pitch deck will help orient your customer to what GitLab is and how we can help them. 

<h4>3   Align Customer with an Existing Case Study</h4>


Aligns the customer needs to an existing GitLab Case study. To locate a good case study to include go to [Customer Reference Documents](https://docs.google.com/document/d/1gwLqlJKiiDxqsIaJ5H_NLsRgDD32cKO1-4wS9ivBijs/ed). You will need to build a few slides that distill the main points of the case study as it aligns with the customer you are pitching to. If you need additional assistance contact Kim Lock at [klock@gitlab.com](mailto:klock@gitlab.com), 

<h4>4 GitLab Demonstration Inclusion?</h4>

Should you include the demonstration within the same meeting as the sales pitch? Typically 50% of the time the demonstration is presented in the same meeting with the pitch deck being delivered first. 

However, this is an audible ready moment where the** AE/SAL and SA need to meet as a team and discuss** if the customer is ready for the demonstration. This should be an agenda item at the regular account management meetings and each team needs to set the criteria of whether to combine a pitch deck meeting with a demonstration on a customer by customer basis.

<h4>5 Presenting & Next Steps</h4>

The sales pitch is designed to help customer stakeholder groups understand their common objectives and assists them to plan to achieve them without being overtly involved in the decision making process. It creates urgency by showing the customer that the pain associated with not partnering with GitLab is greater than any pain associated with moving forward (3 Whys: Why Gitlab, Why Now, Why Do Anything At All). 

<h5>Standard</h5>

Your standard as a sales representative is to present compelling information in a relevant, tailored, and logical way to be intellectually and emotionally persuasive and is unique to a customer's problem or business goal in order to demonstrate CoM.

<h5>Getting the Commit</h5>

When you can do this properly, you will have made a consultative based case that compels the customer to make a decision to buy. At the end of the sales pitch and/or demonstration you can ask about next steps and acquire the verbal commit from the customer. Upon getting the commit you need to accomplish the following tasks. 


1. Set next steps and dates with the customer to move into the Deal Closure phase.
2. Update the opportunities’ stage and status in Salesforce from Proposal to Negotiating
3. Update the command plan
4. Update Clari with the latest forecasting projections based on your most recent sales motions.

At this point the opportunity now moves the **Deal Closure** phase, phase three of the GitLab sales cycle. 

