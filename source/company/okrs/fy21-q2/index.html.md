---
layout: markdown_page
title: "FY21-Q2 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from May 1, 2020 to July 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-03-30 | CEO pushes top goals to this page |
| -4 | 2020-04-06 | E-group pushes updates to this page and discusses it in the E-Group weekly meeting |
| -2 | 2020-04-20 | E-group 50 minute draft review meeting |
| -2 | 2020-04-20 | Discuss with the teams |
| -1 | 2020-04-27 | CEO reports give a How to Achieve presentation |
| 0  | 2020-05-04 | CoS updates OKR page for current quarter to be active |
| +2 | 2020-05-17 | Review previous and next quarter during the next Board meeting |

## OKRs

### 1. CEO: IACV
[Epic 408](https://gitlab.com/groups/gitlab-com/-/epics/408)

1. **CEO KR:** Increase [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) per sales and marketing dollar spend. [IACV efficiency](/handbook/sales/#iacv-efficiency-ratio) > 1.0.
   1.  CRO: Overcome Covid-19 economic impact and hit Q2 iACV and new logo target [Epic 453](https://gitlab.com/groups/gitlab-com/-/epics/453 )
       1.  CRO KR: Close plan conversation by end of May for 80% of Top XX Q2 renewals
       1.  CRO KR: Hit new logo target in each segment (critical for next year) >= 100%
       1.  CRO KR: Build Q3 Pipeline to 2.5x Q3 iACV target
   1.  CRO: Increase sales efficiency [Epic 454](https://gitlab.com/groups/gitlab-com/-/epics/454)
       1.  CRO KR: Web portal KR TBD
       1.  CRO KR: Hire two Large Enterprise ISRs for efficiency proof case
       1.  CRO KR: 3rd KR under discussion
   1. CMO: Strengthen inbound marketing core [KRs in Epic 896](https://gitlab.com/groups/gitlab-com/marketing/-/epics/896)
   1. CMO: Make virtual events excellent [KRs in Epic 401](https://gitlab.com/groups/gitlab-com/-/epics/401)
1. **CEO KR:** More efficient marketing through 'website first' content, reusablility and process improvements.
    1. CMO: Gitlab.tv MVC launched [KRs in Epic 920](https://gitlab.com/groups/gitlab-com/marketing/-/epics/920)
    1. CMO: Marketing Efficiency Improvements aka "The Boring Project" [KRs in Epic 402](https://gitlab.com/groups/gitlab-com/-/epics/402)
    1. CMO: Marketing Project Management Simplification [KRs in Epic 403](https://gitlab.com/groups/gitlab-com/-/epics/403)
1. **CEO KR:** Drive [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) with growth initiatives. $xm generated.

### 2. CEO: Popular next generation product
[Epic 410](https://gitlab.com/groups/gitlab-com/-/epics/410)

1. **CEO KR:** Using an extra stage triples conversion. Increase [SpU](/handbook/product/metrics/#stages-per-user-spu) by 0.5 stages.
1. **CEO KR:** Deliver more value to users efficiently. Release posts items per engineer +25%
1. **CEO KR:** Reduce the cost of GitLab.com. to xxx [per user](/handbook/engineering/infrastructure/performance-indicators/#infrastructure-hosting-cost-per-gitlab-com-monthly-active-users)

### 3. CEO: Great team
[Epic 411](https://gitlab.com/groups/gitlab-com/-/epics/411)

1. **CEO KR:** On average team-members have 3 [Competencies](/handbook/competencies/) certifications.
    1. CMO: Training SDRs to be DevOps & Product Specialists [KRs in Epic 404](https://gitlab.com/groups/gitlab-com/-/epics/404)
1. **CEO KR:** More diverse company. Top of funnel is twice as diverse as the current [identity data](/company/culture/inclusion/identity-data/).
   1.  CRO: Hire top talent on time to plan. [Epic 452](https://gitlab.com/groups/gitlab-com/-/epics/452)
       1.  CRO KR: Top of funnel is twice as diverse as the current identity data
       1.  CRO KR: Weekly hiring forecast and candidate pool review in sales forecast call format
       1.  CRO KR: Roll out second iteration of Field Competencies talent model to raise the talent bar
1. **CEO KR:** Metric driven. All KPIs in operational in Sisense, GitLab, or the handbook. Key meeting presentations have auto-updating graphs through the [automated KPI Slides](/handbook/finance/key-meetings/#automated-kpi-slides) or by [leveraging Sisense in Google Slides](/handbook/finance/key-meetings/#leveraging-sisense-in-google-slides).
1. CFO: Build the enterprise grade systems and processes to drive accounting close process to achieve a 10 day close by end of FY21.
    1. CFO KR: Close in 12 days by end of FY21-Q2
    1. CFO KR: Monthly financials issued under 606.
1. CFO: Build public company ready financial reporting capability.
    1. CFO KR: Complete audits for FY19 and FY20
1. CFO: Improve financial performance.
    1. CFO  KR: Complete prelim operating plan for FY22, validated by eGroup, that places us among the top quartile of identified comparable public companies.


## How to Achieve Presentations

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Product Strategy
* Sales
