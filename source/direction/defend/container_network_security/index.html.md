---
layout: markdown_page
title: "Category Direction - Container Network Security"
---

- TOC
{:toc}

## Description
Containers are a critical part of modern applications. They contain the business logic and data needed to operate, and are frequently spun up and down based on load, costs, and other factors. Because containers are such a critical part of apps, they need to be defended from attacks and inappropriate use. This is even more important when one app is on the same cluster as another app.

### Goal
GitLab's goal with container network security is that any app can be run on a cluster with any other app, with confidence that it can properly identify and stop any unintended use or traffic.
Our goal is also to be able to detect rogue containers and other resources inappropriately added to clusters, so they can either be deleted or blacklisted.
Finally, our goal is to be able to do everything and provide informative results outwards as either Issues or Vulnerabilities, so the security team and others in your organization can act to remediate existing issues and prevent future issues.

### Roadmap
[Planned to Viable](https://gitlab.com/groups/gitlab-org/-/epics/2108)

## What's Next & Why
In 12.10 we are some basic [statistics](https://gitlab.com/gitlab-org/gitlab/issues/32365) showing how much traffic is being allowed vs blocked.  Additionally we are enabling users to [export the logs to their SIEM](https://gitlab.com/gitlab-org/gitlab/-/issues/199666) or central logging solution.

Our next step is to provide a location in the UI for management controls as well as easy access to instructions on how to turn Cilium Network Policies [on or off](https://gitlab.com/gitlab-org/gitlab/-/issues/207322).

## Competitive Landscape
Current solutions that offer container network security are point solutions.  GitLab can differentiate from other offerings by providing security that is embedded into GitLab managed Kubernetes clusters and tightly integrated into the rest of the GitLab product.  Some of the current offerings are free, while others are proprietary.

Some of the solutions that provide container network security include the following products: (list taken from [eSecurity Planet](https://www.esecurityplanet.com/products/top-container-and-kubernetes-security-vendors.html))
*  Alert Logic
*  Anchore
*  Aporeto
*  Aqua Security
*  Capsule8
*  NeuVector
*  Qualys
*  StackRox
*  Sysdig
*  Twistlock

Additionally, [Cilium](https://github.com/cilium/cilium) and [Calico](https://www.projectcalico.org/) are popular open source projects that provide Container Network Security capabilities.  GitLab has embedded [Cilium](https://github.com/cilium/cilium) into GitLab to allow users to create [Network Policies](https://docs.gitlab.com/ee/user/clusters/applications.html#install-cilium-using-gitlab-cicd).

## Analyst Landscape
This category is part of the market defined by Gartner as the [Cloud Workload Protection Platforms (CWPP) Market](https://www.gartner.com/en/documents/3906670/market-guide-for-cloud-workload-protection-platforms).

## Top Customer Success/Sales Issue(s)
As this category is new, we do not yet have top customer success stories or sales issues.

## Top Customer Issue(s)
Currently we do not have customer issues related to this category.

## Top Vision Item(s)
Long-term we envision integrating a Network Intrusion Detection System (IDS) with Cilium to allow for advanced signature blocking and network anomaly detection.
